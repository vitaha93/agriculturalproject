package com.introlabsystems.amazone;

import com.introlabsystems.CsvTools;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vitalii on 1/18/16.
 */
public class Crawler {

    //    private List<String>urls = Arrays.asList("http://et2.amazone.de/etnav.asp?navi=732",
//            "http://et2.amazone.de/etnav.asp?navi=733",
//            "http://et2.amazone.de/etnav.asp?navi=734"
//            "http://et2.amazone.de/etnav.asp?navi=740"
//            "http://et2.amazone.de/etnav.asp?navi=77",
//            "http://et2.amazone.de/etnav.asp?navi=1616",
//            "http://et2.amazone.de/etnav.asp?navi=735",
//            "http://et2.amazone.de/etnav.asp?navi=1897",
//    )
    private static String FIRST_URL = "http://et2.amazone.de/etnav.asp";
    private static String productTypeSelector = "ul > li > a";
    private static String modelTypeSelector = "ul > li > a[href*=navi]";
    private static String additionalModelDataSelector = "ul > li > a[href*=navi]";
    private static String modelNameSelector = "select option";
    static List<Item> items = new ArrayList<>();

    public List<Item> scrape() {

        Document doc = null;
        try {
            doc = Jsoup.connect(FIRST_URL).get();

            for (Element prodType : doc.select(productTypeSelector)) {
                if (doc.select(productTypeSelector).indexOf(prodType) < 3) {
                    continue;
                }
                Document document = Jsoup.connect(prodType.attr("abs:href")).get();
                final String productType = prodType.text();
                //System.out.println(prodType.attr("abs:href"));
                for (Element modelType : document.select(modelTypeSelector)) {

                    Document document2 = Jsoup.connect(modelType.attr("abs:href")).get();
                    final String itemModelType = modelType.text();
                    //System.out.println(modelType.attr("abs:href"));
                    System.out.println("-" + Thread.currentThread().getId() + "   " + itemModelType);

                    for (Element modelData : document2.select(additionalModelDataSelector)) {
                        if (doc.select(productTypeSelector).indexOf(prodType) < 4
                                && document.select(modelTypeSelector).indexOf(modelType) < 2
                                && document2.select(additionalModelDataSelector).indexOf(modelData) < 4) {
                            continue;
                        }
                        String modelUrl = modelData.attr("abs:href");
                        final String addData = modelData.text();
                        Document document3 = Jsoup.connect(modelUrl).ignoreHttpErrors(true).get();
                        System.out.println("---" + Thread.currentThread().getName() + "   " + addData);
                        for (Element modelName : document3.select(modelNameSelector)) {
                            // System.out.println(modelUrl + "&hg=" + modelName.val());

                            final String itemModelname = modelName.text();
                            Document document4 = Jsoup.connect(modelUrl + "&hg=" + modelName.val()).ignoreHttpErrors(true).get();
                            String image = document4.select("div > img").attr("abs:src");
                            final String itemImage = downloadImage(image);
                            //System.out.println(image);
                            try {
                                Document document5 = Jsoup.connect("http://et2.amazone.de/" + document4.select("a[target=liste]").attr("href")).ignoreHttpErrors(true).get();
                                System.out.println("------" + Thread.currentThread().getName() + "   " + itemModelname);
                                for (Element element : document5.select("form[name=\"teileliste\"] tr:has(a)")) {
                                    final Item item = new Item();
                                    item.setProductType(productType);
                                    item.setModelType(itemModelType);
                                    item.setModelName(itemModelname);
                                    item.setImage(itemImage);
                                    item.setAddModelData(addData);
                                    item.setPartNo(element.select("td:nth-of-type(2)").text());
                                    item.setDescription(element.select("td:nth-of-type(4)").text());
                                    items.add(item);

                                }
                            } catch (HttpStatusException e) {

                            }

                        }
                    }
                }
                new CsvTools().writeNewData(items, prodType.text());

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return items;
    }

    private String downloadImage(String imageUrl) {
        URL url = null;
        String imageName = "";
        try {
            url = new URL(imageUrl);
            String fileName = url.getFile();

            imageName = StringUtils.substringAfterLast(fileName, "/");
            String destName = "/home/vitalii/amazonepics/" + imageName;
            try {
                FileUtils.copyURLToFile(url, new File(destName));
            } catch (IOException ex) {
                System.out.println("Image does not save, url: " + url);
                ex.printStackTrace();
                return "";
            }
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        }


        return imageName;
    }

}
