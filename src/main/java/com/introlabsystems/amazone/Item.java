package com.introlabsystems.amazone;

/**
 * Created by vitalii on 1/18/16.
 */
public class Item {
    private String productType;
    private String modelType;
    private String modelName;
    private String addModelData;
    private String whereItFits;
    private String subAssembly;
    private String partNo;
    private String description;
    private String image;
    private String dimension;

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getModelType() {
        return modelType;
    }

    public void setModelType(String modelType) {
        this.modelType = modelType;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getAddModelData() {
        return addModelData;
    }

    public void setAddModelData(String addModelData) {
        this.addModelData = addModelData;
    }

    public String getWhereItFits() {
        return whereItFits;
    }

    public void setWhereItFits(String whereItFits) {
        this.whereItFits = whereItFits;
    }

    public String getSubAssembly() {
        return subAssembly;
    }

    public void setSubAssembly(String subAssembly) {
        this.subAssembly = subAssembly;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    @Override
    public String toString() {
        return "Item{" +
                "productType='" + productType + '\'' +
                ", modelType='" + modelType + '\'' +
                ", modelName='" + modelName + '\'' +
                ", addModelData='" + addModelData + '\'' +
                ", whereItFits='" + whereItFits + '\'' +
                ", subAssembly='" + subAssembly + '\'' +
                ", partNo='" + partNo + '\'' +
                ", description='" + description + '\'' +
                ", image='" + image + '\'' +
                ", dimension='" + dimension + '\'' +
                '}';
    }


}
