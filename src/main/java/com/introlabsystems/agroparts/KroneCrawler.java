package com.introlabsystems.agroparts;

import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.introlabsystems.CsvTools;
import com.introlabsystems.amazone.Item;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by vitalii on 1/20/16.
 */
public class KroneCrawler {


    private final static Logger logger = Logger.getLogger(KroneCrawler.class);
    final List<Item> catItems = new ArrayList<>();
    static int picCount;
    static int urlCount;
    private static Map<String, String> cookies = new TreeMap<>();

    public static void main(String[] args) throws Exception {
        KroneCrawler crawler = new KroneCrawler();
        crawler.login();
        scrape();
        //getLinks();


    }

    private static void scrape() throws InterruptedException {

        ExecutorService executor = Executors.newFixedThreadPool(300);

        KroneCrawler kroneCrawler = new KroneCrawler();

        List<LinkItem> links = new CsvTools().getLinksCsvUsingScanner("krone_links.csv");

        for (int index = 0; index < links.size(); index += 250) {

            final int finalIndex = index;
            Runnable task = new Thread(new Runnable() {
                public void run() {
                    for (int i = finalIndex; i < finalIndex + 250; i += 1) {
                        if (i < links.size())
                            kroneCrawler.scrapeItems(links.get(i), links.get(i).getLink());
                        //System.out.println(Thread.currentThread().getName());
                        synchronized (this) {
                            urlCount++;
                        }
                        System.out.println(urlCount);
                        System.out.println(Thread.activeCount());
                    }
                    System.err.println(Thread.currentThread().getName() + " finished!!!");
                }
            });

            executor.execute(task);
        }

        executor.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);

        System.out.println(kroneCrawler.catItems.size());
        new CsvTools().writeNewData(kroneCrawler.catItems, "kroneUK/kroneUK");
    }


    private static String parseJSONImage(String jsonLine) {
        System.out.println(jsonLine);
        JsonElement jelement = new JsonParser().parse(jsonLine.trim());
        JsonObject jobject = jelement.getAsJsonObject();
        //System.out.println(jsonLine);
        return jobject.getAsJsonArray("entries").get(0)
                .getAsJsonObject().getAsJsonArray("hotspots").get(0)
                .getAsJsonObject()
                .getAsJsonPrimitive("file")
                .getAsString();

    }

    private static JsonArray parseJSON(String jsonLine) {
        JsonElement jelement = new JsonParser().parse(jsonLine);
        JsonObject jobject = jelement.getAsJsonObject();

        return jobject.getAsJsonArray("entries");
    }

    private String downloadImage(String imageUrl, String imageName) throws IOException {

        String destName = "/home/vitalii/agroparts/krone/" + imageName;
        if (new java.io.File(destName).exists()) {
            System.out.println("Image exist!");
            return imageName;
        }

        Connection.Response resultImageResponse = Jsoup.connect(imageUrl).cookies(cookies)
                .ignoreContentType(true).timeout(0).execute();

        FileOutputStream out = (new FileOutputStream(new java.io.File(destName)));
        out.write(resultImageResponse.bodyAsBytes());
        out.close();
        synchronized (this) {
            picCount++;
        }
        return imageName;
    }


    private void scrapeItems(LinkItem linkItem, String url) {
        try {
//            try {Thread.sleep(2000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            Document document = Jsoup.connect(url).userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:43.0) Gecko/20100101 Firefox/43.0").cookies(cookies).ignoreContentType(true).ignoreHttpErrors(true).timeout(0).get();
            String imageUrlTemplate = "https://www.agroparts.com/ip40_krone-ldm/imagedata?path=kroneldm/kroneldm1/drawings/%s&rnd=2605&request=GetImage&format=image/png&bbox=0,0,2759,1446&width=1366&height=768&scalefac=0.3&ticket=&cv=1";
            final String imageName = parseJSONImage(document.text());
            downloadImage(String.format(imageUrlTemplate, imageName), StringUtils.substringBefore(imageName, "."));
            final String prodType = linkItem.getProductType();
            final String modelType = linkItem.getModelType();
            final String modelName = linkItem.getModelName();
            final String whereItFits = linkItem.getWhereItFits();
            final String addModelData = linkItem.getAddModelData();
            for (JsonElement jsonElement : parseJSON(document.text().trim())) {

                if (jsonElement.getAsJsonObject().has("linkId") && !jsonElement.getAsJsonObject().get("linkId").isJsonNull()) {
                    final String linkId = jsonElement.getAsJsonObject().getAsJsonPrimitive("linkId").getAsString();

                    scrapeItems(linkItem, url + "/" + linkId);
                } else {
                    final Item item = new Item();
                    item.setProductType(prodType);
                    item.setModelName(modelName);
                    item.setModelType(modelType);
                    item.setAddModelData(addModelData);
                    item.setWhereItFits(whereItFits);
                    item.setImage(StringUtils.substringBefore(imageName, "."));

                    if (jsonElement.getAsJsonObject().getAsJsonObject("fields").has("partNumber") &&
                            !jsonElement.getAsJsonObject().getAsJsonObject("fields").get("partNumber").isJsonNull()) {
                        final String partNo = jsonElement.getAsJsonObject().getAsJsonObject("fields").getAsJsonPrimitive("partNumber").getAsString();
                        item.setPartNo(partNo);
                        if (jsonElement.getAsJsonObject().getAsJsonObject("fields").has("description") &&
                                !jsonElement.getAsJsonObject().getAsJsonObject("fields").get("description").isJsonNull()) {
                            final String description = jsonElement.getAsJsonObject().getAsJsonObject("fields").getAsJsonPrimitive("description").getAsString();
                            item.setDescription(description);
                            synchronized (this) {
                                catItems.add(item);
                            }
                        }
                        //System.out.println(item);
                    }
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void login() throws IOException {
        logger.info("Login..");
        String username = "simbirley";
        String password = "ePOerIkmjH";

        WebClient webClient = JsoupBaseScraper.getWebClient();
        webClient.addRequestHeader("Referer", "https://www.agroparts.com/agroparts/login2.jsp");
        String formData = "action=login&user_name=" + username + "&user_pwd=" + password + "&submit1=Login";
        WebRequest webRequest = new WebRequest(new URL("https://www.agroparts.com/agroparts/login"), HttpMethod.POST);
        webRequest.setRequestBody(formData);
        webClient.getPage(webRequest);

        webClient.getCookieManager().getCookies()
                .forEach(cookie -> cookies.put(cookie.getName(), cookie.getValue()));

    }


    private static List<LinkItem> linkItemList = new ArrayList<>();

    public static void getLinks() throws IOException {
        logger.info("Login..");
        Document document = Jsoup.connect("https://www.agroparts.com/ip40_krone-ldm/data/navigation?location=&ts=1453819035944")
                .cookies(cookies).ignoreContentType(true).get();
        for (JsonElement jsonElement : parseJSON(document.text())) {
            String firstLevelUrlTemlape = "https://www.agroparts.com/ip40_krone-ldm/data/navigation?ts=1453819035944&location=";
            final String firsLevelLink = firstLevelUrlTemlape + jsonElement.getAsJsonObject().getAsJsonPrimitive("linkId").getAsString();
            final String firsLevelLabel = jsonElement.getAsJsonObject().getAsJsonPrimitive("label").getAsString();
            System.out.println(firsLevelLink);
            final Document page2 = Jsoup.connect(firsLevelLink)
                    .cookies(cookies)
                    .ignoreContentType(true)
                    .get();
            for (JsonElement element2 : parseJSON(page2.text())) {
                final String secondLevelLink = firsLevelLink + "/" + element2.getAsJsonObject().getAsJsonPrimitive("linkId").getAsString();
                final String secondLevelLabel = element2.getAsJsonObject().getAsJsonPrimitive("label").getAsString();
                System.out.println(secondLevelLink);
                final Document page3 = Jsoup.connect(secondLevelLink)
                        .cookies(cookies)
                        .ignoreContentType(true)
                        .get();
                for (JsonElement element3 : parseJSON(page3.text())) {
                    final String thirdLevelLink = secondLevelLink + "/" + element3.getAsJsonObject().getAsJsonPrimitive("linkId").getAsString();
                    final String thirdLevelLabel = element3.getAsJsonObject().getAsJsonPrimitive("label").getAsString();
                    System.out.println(thirdLevelLink);
                    final Document page4 = Jsoup.connect(thirdLevelLink)
                            .cookies(cookies)
                            .ignoreContentType(true)
                            .get();
                    for (JsonElement element4 : parseJSON(page4.text())) {
                        final String fourthLevelLink = thirdLevelLink + "/" + element4.getAsJsonObject().getAsJsonPrimitive("linkId").getAsString();
                        final String fourthLevelLabel = element4.getAsJsonObject().getAsJsonPrimitive("label").getAsString();
                        final Document page5 = Jsoup.connect(fourthLevelLink)
                                .cookies(cookies)
                                .ignoreContentType(true)
                                .get();
                        for (JsonElement element5 : parseJSON(page5.text())) {
                            final String fivesLevelLink = fourthLevelLink + "/" + element5.getAsJsonObject().getAsJsonPrimitive("linkId").getAsString();
                            final String fivesLevelLabel = element5.getAsJsonObject().getAsJsonPrimitive("label").getAsString();
                            final Document page6 = Jsoup.connect(fivesLevelLink)
                                    .cookies(cookies)
                                    .ignoreContentType(true)
                                    .get();
                            for (JsonElement element6 : parseJSON(page6.text())) {
                                final String sixLevelLink = fivesLevelLink + "/" + element6.getAsJsonObject().getAsJsonPrimitive("linkId").getAsString();
                                final LinkItem linkItem = new LinkItem();
                                linkItem.setProductType(firsLevelLabel);
                                linkItem.setModelType(secondLevelLabel);
                                linkItem.setModelName(thirdLevelLabel);
                                linkItem.setWhereItFits(fourthLevelLabel);
                                linkItem.setAddModelData(fivesLevelLabel);
                                linkItem.setLink(sixLevelLink);
                                linkItemList.add(linkItem);

                            }

                        }
                    }
                }
            }
        }
        new CsvTools().writeLinks(linkItemList);
        System.out.println();
    }
}
