package com.introlabsystems.agroparts;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.logging.Level;

/**
 * @author Anton Goncharov
 */
public class JsoupBaseScraper {
    private static final Logger logger = Logger.getLogger(JsoupBaseScraper.class);
    private static final String userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36";
    private static WebClient webClient;

    static {
        webClient = new WebClient(BrowserVersion.CHROME);
        webClient.getOptions().setJavaScriptEnabled(true);
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
        webClient.getOptions().setCssEnabled(true);

        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(Level.OFF);
    }

    public static WebClient getWebClient() {
        return webClient;
    }

    public static Document getDocument(String url) {
        Document document = null;
        try {
            document = Jsoup.connect(url)
                    .userAgent(userAgent)
                    .get();
        } catch (IOException e) {
            logger.error(String.format("Error getting web page Document from URL %s: %s", url, e));
        }

        return document;
    }

    public static Document getWebClientDocument(String url) {
        Document document = null;

        try {
            String htmlSource = webClient.getPage(url).getWebResponse().getContentAsString();

            document = Jsoup.parse(htmlSource);
        } catch (IOException e) {
            logger.error(String.format("Error getting web page Document from URL %s: %s", url, e));
        }

        return document;
    }

    protected static Element getFirstElement(Element element, String cssQuery) {
        return element.select(cssQuery).first();
    }

    protected static Element getLastElement(Element element, String cssQuery) {
        return element.select(cssQuery).last();
    }

    protected static Element getElement(Element element, String cssQuery, int index) {
        return element.select(cssQuery).get(index);
    }

    protected static Elements getElements(Element element, String cssQuery) {
        return element.select(cssQuery);
    }

    protected static String getText(Element element) {
        if (element != null && !element.text().isEmpty()) {
            return element.text().trim();
        }
        return "N/A";
    }

    protected static String getElementURL(Element element, String attribute) {
        if (element != null && attribute.equals("href")) {
            return element.attr("href");
        } else if (element != null && attribute.equals("src")) {
            return element.attr("src");
        }

        return null;
    }

    protected static String getElementAbsURL(Element element, String attribute) {
        if (element != null && attribute.equals("href")) {
            return element.attr("abs:href");
        } else if (element != null && attribute.equals("src")) {
            return element.attr("abs:src");
        }
        return null;
    }
}
