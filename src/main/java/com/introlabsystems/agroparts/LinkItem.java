package com.introlabsystems.agroparts;

/**
 * Created by vitalii on 1/20/16.
 */
public class LinkItem {
    private String link;
    private String productType;
    private String modelType;
    private String modelName;
    private String addModelData;
    private String whereItFits;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getModelType() {
        return modelType;
    }

    public void setModelType(String modelType) {
        this.modelType = modelType;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getAddModelData() {
        return addModelData;
    }

    public void setAddModelData(String addModelData) {
        this.addModelData = addModelData;
    }

    public String getWhereItFits() {
        return whereItFits;
    }

    public void setWhereItFits(String whereItFits) {
        this.whereItFits = whereItFits;
    }

    @Override
    public String toString() {
        return "LinkItem{" +
                "link='" + link + '\'' +
                ", productType='" + productType + '\'' +
                ", modelType='" + modelType + '\'' +
                ", modelName='" + modelName + '\'' +
                ", addModelData='" + addModelData + '\'' +
                ", whereItFits='" + whereItFits + '\'' +
                '}';
    }
}
