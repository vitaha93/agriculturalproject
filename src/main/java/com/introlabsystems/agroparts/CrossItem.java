package com.introlabsystems.agroparts;

/**
 * Created by vitalii on 1/26/16.
 */
public class CrossItem {

    private String modelCount;
    private String modelNames;
    private String partNo;
    private String description;


    public String getModelCount() {
        return modelCount;
    }

    public void setModelCount(String modelCount) {
        this.modelCount = modelCount;
    }

    public String getModelNames() {
        return modelNames;
    }

    public void setModelNames(String modelNames) {
        this.modelNames = modelNames;
    }


    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public String toString() {
        return "Item{" +
                ", modelCount='" + modelCount + '\'' +
                ", modelNames='" + modelNames + '\'' +
                ", partNo='" + partNo + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

}
