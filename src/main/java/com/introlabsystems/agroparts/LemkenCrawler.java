package com.introlabsystems.agroparts;

import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.introlabsystems.CsvTools;
import com.introlabsystems.amazone.Item;
import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by vitalii on 1/20/16.
 */
public class LemkenCrawler {


    private final static Logger logger = Logger.getLogger(LemkenCrawler.class);
    final List<Item> catItems = new ArrayList<>();
    static int threadCount;
    static int picCount;

    public static void main(String[] args) throws IOException {
        LemkenCrawler lemkenCrawler = new LemkenCrawler();
        lemkenCrawler.scrape();


    }

    private void scrape() {

        LemkenCrawler lemkenCrawler = new LemkenCrawler();
        try {
            lemkenCrawler.login();
            System.out.println("logged");
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<LinkItem> links = new CsvTools().getLinksCsvUsingScanner("lemken_links.csv");


        final List<Thread> threads = new ArrayList<>();


        for (int index = links.size() / 2; index < links.size(); index += 1000) {

            final int finalIndex = index;
            threads.add(new Thread(new Runnable() {
                public void run() {
                    for (int i = finalIndex; i < finalIndex + 1000; i += 1) {
                        if (i < links.size())
                            lemkenCrawler.scrapeItems(links.get(i), links.get(i).getLink());
                        //System.out.println(Thread.currentThread().getName());
                        //System.out.println(i);
                    }
                    //threadCount--;
                }
            }));

        }
        for (Thread thread : threads) {
            thread.start();
        }
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        System.out.println(lemkenCrawler.catItems.size());
        new CsvTools().writeNewData(lemkenCrawler.catItems, "Lemken-second-");
    }

    private static List<String> firstLevelUrls = new ArrayList<>();
    private static List<String> secondLevelUrls = new ArrayList<>();
    private static List<String> thirdLevelUrls = new ArrayList<>();
    private static List<String> foursLevelUrls = new ArrayList<>();
    private static List<String> fivesLevelUrls = new ArrayList<>();
    private static Map<String, String> cookies = new TreeMap<>();
    private static List<LinkItem> linkItemList = new ArrayList<>();


    private static String firstLevelUrlTemlape = "https://www.agroparts.com/ip40_lemken/data/navigation?ts=1453296604875&location=";


    public static void getLinks() throws IOException {
        logger.info("Login..");
        String username = "simbirley";
        String password = "ePOerIkmjH";

        WebClient webClient = JsoupBaseScraper.getWebClient();
        webClient.addRequestHeader("Referer", "https://www.agroparts.com/agroparts/login2.jsp");
        String formData = "action=login&user_name=" + username + "&user_pwd=" + password + "&submit1=Login";
        WebRequest webRequest = new WebRequest(new URL("https://www.agroparts.com/agroparts/login"), HttpMethod.POST);
        webRequest.setRequestBody(formData);
        webClient.getPage(webRequest);

        webClient.getCookieManager().getCookies()
                .forEach(cookie -> cookies.put(cookie.getName(), cookie.getValue()));

        Document document = Jsoup.connect("https://www.agroparts.com/ip40_lemken/data/navigation?location=&ts=1453293070848")
                .cookies(cookies).ignoreContentType(true).get();
        for (JsonElement jsonElement : parseJSON(document.text())) {
            final String firsLevelLink = firstLevelUrlTemlape + jsonElement.getAsJsonObject().getAsJsonPrimitive("linkId").getAsString();
            final String firsLevelLabel = jsonElement.getAsJsonObject().getAsJsonPrimitive("label").getAsString();
            System.out.println(firsLevelLink);
            final Document page2 = Jsoup.connect(firsLevelLink)
                    .cookies(cookies)
                    .ignoreContentType(true)
                    .get();
            for (JsonElement element2 : parseJSON(page2.text())) {
                final String secondLevelLink = firsLevelLink + "/" + element2.getAsJsonObject().getAsJsonPrimitive("linkId").getAsString();
                final String secondLevelLabel = element2.getAsJsonObject().getAsJsonPrimitive("label").getAsString();
                System.out.println(secondLevelLink);
                final Document page3 = Jsoup.connect(secondLevelLink)
                        .cookies(cookies)
                        .ignoreContentType(true)
                        .get();
                for (JsonElement element3 : parseJSON(page3.text())) {
                    final String thirdLevelLink = secondLevelLink + "/" + element3.getAsJsonObject().getAsJsonPrimitive("linkId").getAsString();
                    final String thirdLevelLabel = element3.getAsJsonObject().getAsJsonPrimitive("label").getAsString();
                    System.out.println(thirdLevelLink);
                    final Document page4 = Jsoup.connect(thirdLevelLink)
                            .cookies(cookies)
                            .ignoreContentType(true)
                            .get();
                    for (JsonElement element4 : parseJSON(page4.text())) {
                        final String fourthLevelLink = thirdLevelLink + "/" + element4.getAsJsonObject().getAsJsonPrimitive("linkId").getAsString();
                        final String fourthLevelLabel = element4.getAsJsonObject().getAsJsonPrimitive("label").getAsString();
                        System.out.println(fourthLevelLabel);
                        final Document page5 = Jsoup.connect(fourthLevelLink)
                                .cookies(cookies)
                                .ignoreContentType(true)
                                .get();

                        for (JsonElement element5 : parseJSON(page5.text())) {
                            final String fivesLevelLink = fourthLevelLink + "/" + element5.getAsJsonObject().getAsJsonPrimitive("linkId").getAsString();
                            final String fivesLevelLabel = element5.getAsJsonObject().getAsJsonPrimitive("label").getAsString();

                            final LinkItem linkItem = new LinkItem();
                            linkItem.setProductType(firsLevelLabel);
                            linkItem.setModelType(secondLevelLabel);
                            linkItem.setModelName(thirdLevelLabel);
                            linkItem.setWhereItFits(fourthLevelLabel);
                            linkItem.setAddModelData(fivesLevelLabel);
                            linkItem.setLink(fivesLevelLink);

                            linkItemList.add(linkItem);

                        }
                    }
                }
            }


        }
        new CsvTools().writeLinks(linkItemList);

        System.out.println();

//        }
    }

    private static String parseJSONImage(String jsonLine) {
        JsonElement jelement = new JsonParser().parse(jsonLine);
        JsonObject jobject = jelement.getAsJsonObject();
        //System.out.println(jsonLine);
        return jobject.getAsJsonArray("entries").get(0)
                .getAsJsonObject().getAsJsonArray("hotspots").get(0)
                .getAsJsonObject()
                .getAsJsonPrimitive("file")
                .getAsString();

    }

    private static JsonArray parseJSON(String jsonLine) {
        JsonElement jelement = new JsonParser().parse(jsonLine);
        JsonObject jobject = jelement.getAsJsonObject();
        List<String> ids = new ArrayList<>();

        return jobject.getAsJsonArray("entries");
    }

    private String downloadImage(String imageUrl, String imageName) throws IOException {

        //System.out.println(imageName);
        String destName = "/home/vitalii/agroparts/lemken/" + imageName;
        if (new java.io.File(destName).exists()) {
            System.out.println("Image exist!");
            return imageName;
        }

        Connection.Response resultImageResponse = Jsoup.connect(imageUrl).cookies(cookies)
                .ignoreContentType(true).timeout(0).execute();

// output here
        FileOutputStream out = (new FileOutputStream(new java.io.File(destName)));
        out.write(resultImageResponse.bodyAsBytes());  // resultImageResponse.body() is where the image's contents are.
        out.close();
        synchronized (this) {
            picCount++;
        }
        //System.out.println(picCount);
        return imageName;
    }


    private void scrapeItems(LinkItem linkItem, String url) {
        try {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Document document = Jsoup.connect(url).cookies(cookies).ignoreContentType(true).ignoreHttpErrors(true).get();
            //System.out.println(document);
            String imageUrlTemplate = "https://www.agroparts.com/ip40_lemken/imagedata?path=lemken/lemken2/drawings/%s.tif&rnd=2605&request=GetImage&format=image/png&bbox=0,0,2759,1446&width=1366&height=768&scalefac=0.5&ticket=&cv=1";
            final String imageName = parseJSONImage(document.text());
            downloadImage(String.format(imageUrlTemplate, imageName), imageName);
            final String prodType = linkItem.getProductType();
            final String modelType = linkItem.getModelType();
            final String modelName = linkItem.getModelName();
            final String whereItFits = linkItem.getWhereItFits();
            final String addModelData = linkItem.getAddModelData();
            for (JsonElement jsonElement : parseJSON(document.text())) {

                if (jsonElement.getAsJsonObject().has("linkId") && !jsonElement.getAsJsonObject().get("linkId").isJsonNull()) {
                    final String linkId = jsonElement.getAsJsonObject().getAsJsonPrimitive("linkId").getAsString();

                    scrapeItems(linkItem, url + "/" + linkId);
                } else {
                    final Item item = new Item();
                    item.setProductType(prodType);
                    item.setModelName(modelName);
                    item.setModelType(modelType);
                    item.setAddModelData(addModelData);
                    item.setWhereItFits(whereItFits);
                    item.setImage(imageName);
                    item.setPartNo(jsonElement.getAsJsonObject().getAsJsonObject("fields").getAsJsonPrimitive("partNumber").getAsString());
                    item.setDescription(jsonElement.getAsJsonObject().getAsJsonObject("fields").getAsJsonPrimitive("description").getAsString());
                    item.setDimension(jsonElement.getAsJsonObject().getAsJsonObject("fields").getAsJsonPrimitive("dimension").getAsString());
                    synchronized (this) {
                        catItems.add(item);
                    }
                    System.out.println(catItems.size());
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void login() throws IOException {
        logger.info("Login..");
        String username = "simbirley";
        String password = "ePOerIkmjH";

        WebClient webClient = JsoupBaseScraper.getWebClient();
        webClient.addRequestHeader("Referer", "https://www.agroparts.com/agroparts/login2.jsp");
        String formData = "action=login&user_name=" + username + "&user_pwd=" + password + "&submit1=Login";
        WebRequest webRequest = new WebRequest(new URL("https://www.agroparts.com/agroparts/login"), HttpMethod.POST);
        webRequest.setRequestBody(formData);
        webClient.getPage(webRequest);

        webClient.getCookieManager().getCookies()
                .forEach(cookie -> cookies.put(cookie.getName(), cookie.getValue()));

    }
}
