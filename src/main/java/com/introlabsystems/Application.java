package com.introlabsystems;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by vitalii on 1/12/16.
 */
public class Application {
    public static void main(String[] args) throws IOException {
//        DBConnection dbConnection = new DBConnection();
//        dbConnection.openSqlLiteConnection();
//        List<ResultItem> resultItems = dbConnection.getPartNumbers();
//        new CsvTools().writeData(resultItems);


//        new CsvTools().writeNewData(new LemkenCrawler().scrape(),"Full-");
        //DBConnection dbConnection = new DBConnection();
        //dbConnection.openSqlLiteConnection();
        new ExcelTools().writeSkfItems(new ExcelTools().readSkfItems(), "SKF-parts");

        //checkBan();
    }

    private static void checkBan() throws IOException {
        final List<String> links = Arrays.asList("http://www.realtor.com/realestateandhomes-detail/8556-Verde-Park-Cir_Las-Vegas_NV_89129_M16053-41464",
                "http://www.realtor.com/realestateandhomes-detail/2777-Paradise-Rd-Unit-304_Las-Vegas_NV_89109_M17755-35024",
                "http://www.realtor.com/realestateandhomes-detail/7353-Silver-Spirit-St_Las-Vegas_NV_89131_M23709-47509",
                "http://www.realtor.com/realestateandhomes-detail/1909-High-Valley-Ct-Unit-107_Las-Vegas_NV_89128_M29389-05846",
                "http://www.realtor.com/realestateandhomes-detail/213-Piazza-del-Verano-St_Las-Vegas_NV_89138_M17663-38560");
        int count = 0;
        while (true) {
            for (String link : links) {
                Document document = Jsoup.connect(link).get();
                count++;
                if (count % 100 == 0)
                    System.out.println(count);
                if (StringUtils.containsIgnoreCase(document.text(), "Blocked IP Address")) {
                    System.err.println("Blocket");
                    System.out.println(count);
                    break;

                }

            }

        }

    }
}
