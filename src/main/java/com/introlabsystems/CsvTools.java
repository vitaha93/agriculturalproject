package com.introlabsystems;

import com.introlabsystems.agroparts.CrossItem;
import com.introlabsystems.agroparts.LinkItem;
import com.introlabsystems.amazone.Item;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by vitalii on 1/11/16.
 */
public class CsvTools {

    FileWriter fileWriter;
    CSVPrinter csvFilePrinter;
    private static final Object[] SET_HEADER = {"Part #", "Description", "Model numbers", "Count"};


    public void writeData(List<ResultItem> setItems) {

        CSVFormat csvFileFormat = CSVFormat.DEFAULT
                .withDelimiter(',')
                .withQuote('"')
                .withRecordSeparator("\n");

        try {

            fileWriter = new FileWriter("export.csv");

            csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);

            csvFilePrinter.printRecord(SET_HEADER);

            for (ResultItem item : setItems) {
                List<String> setDataRecord = new ArrayList<>();
                setDataRecord.add(item.getPartNo());
                setDataRecord.add(item.getDescription());
                setDataRecord.add(item.getModelNumbers());
                setDataRecord.add(String.valueOf(item.getCount()));
                csvFilePrinter.printRecord(setDataRecord);
            }
            System.out.println("CSV file was created successfully !!!");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
                csvFilePrinter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter/csvPrinter !!!");
                e.printStackTrace();
            }
        }
    }


    private static final Object[] SET_HEADERS = {"Product type", "Model Type", "Model Name", "Additional Model Data",
            "Where It Fits", "PartNo", "Description", "Image", "Dimension"};

    public void writeNewData(List<Item> setItems, String type) {

        CSVFormat csvFileFormat = CSVFormat.DEFAULT
                .withDelimiter(',')
                .withQuote('"')
                .withRecordSeparator("\n");
        try {

            fileWriter = new FileWriter(type + "export.csv");

            csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);

            csvFilePrinter.printRecord(SET_HEADERS);

            for (Item item : setItems) {

                List<String> setDataRecord = new ArrayList<>();
                setDataRecord.add(item.getProductType());
                setDataRecord.add(item.getModelType());
                setDataRecord.add(item.getModelName());
                setDataRecord.add(item.getAddModelData());
                setDataRecord.add(item.getWhereItFits());
                //setDataRecord.add(item.getSubAssembly());
                setDataRecord.add(item.getPartNo().trim());
                setDataRecord.add(item.getDescription());
                setDataRecord.add(item.getImage());
                setDataRecord.add(item.getDimension());
                csvFilePrinter.printRecord(setDataRecord);
            }
            System.out.println("CSV file was created successfully !!!");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
                csvFilePrinter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter/csvPrinter !!!");
                e.printStackTrace();
            }
        }
    }


    public void writeLinks(List<LinkItem> links) {

        CSVFormat csvFileFormat = CSVFormat.DEFAULT
                .withDelimiter(',')
                .withQuote('"')
                .withRecordSeparator("\n");
        try {

            fileWriter = new FileWriter("krone_links.csv");

            csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);

            // csvFilePrinter.printRecord(SET_HEADERS);

            for (LinkItem item : links) {
                List<String> record = new ArrayList<>();
                record.add(item.getLink());
                record.add(item.getProductType());
                record.add(item.getModelType());
                record.add(item.getModelName());
                record.add(item.getWhereItFits());
                record.add(item.getAddModelData());
                csvFilePrinter.printRecord(record);
            }
            System.out.println("CSV file was created successfully !!!");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
                csvFilePrinter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter/csvPrinter !!!");
                e.printStackTrace();
            }
        }
    }

    public List<LinkItem> getLinksCsvUsingScanner(String filepath) {

        ArrayList<LinkItem> items = new ArrayList<>();
        Scanner reader = null;
        try {
            reader = new Scanner(new File(filepath));
        } catch (Exception ex) {

        }
        List<String[]> records = new ArrayList<>();

        System.out.println(records);

        int i = 0;
        while (reader.hasNextLine()) {
            records.add(reader.nextLine().split(","));
        }

        Iterator<String[]> iterator = records.iterator();
        iterator.next();

        System.out.println(records.size());

        for (String[] record : records) {
            try {
                LinkItem item = new LinkItem();
                item.setLink(record[0]);
                item.setProductType(record[1]);
                item.setModelType(record[2]);
                item.setModelName(record[3]);
                item.setWhereItFits(record[4]);
                item.setAddModelData(record[5]);
                items.add(item);
                //System.out.println(item.getLink());
            } catch (Exception ex) {
                System.out.println(Arrays.asList(record));
                ex.printStackTrace();
                System.out.println("error");
                continue;
            }
        }
        return items.stream().distinct().collect(Collectors.toList());
    }

    public List<CrossItem> getCrossUsingScanner(String filepath) {

        ArrayList<CrossItem> items = new ArrayList<>();
        Scanner reader = null;
        try {
            reader = new Scanner(new File(filepath));
        } catch (Exception ex) {

        }
        List<String[]> records = new ArrayList<>();

        System.out.println(records);

        int i = 0;
        while (reader.hasNextLine()) {
            records.add(reader.nextLine().split(","));
        }

        Iterator<String[]> iterator = records.iterator();
        iterator.next();

        System.out.println(records.size());

        for (String[] record : records) {
            try {
                CrossItem item = new CrossItem();
                item.setPartNo(record[0]);
                item.setDescription(record[1]);
                item.setModelNames(record[2]);
                item.setModelCount(record[3]);
                items.add(item);
                //System.out.println(item.getLink());
            } catch (Exception ex) {
                System.out.println(Arrays.asList(record));
                ex.printStackTrace();
                System.out.println("error");
                continue;
            }
        }
        return items.stream().distinct().collect(Collectors.toList());
    }

    public List<CrossItem> readCsv(String fileName) {

        FileReader fileReader = null;

        CSVParser csvFileParser = null;

        //Create the CSVFormat object with the header mapping
        CSVFormat csvFileFormat = CSVFormat.DEFAULT
                .withDelimiter(',')
                .withQuote('"')
                .withRecordSeparator("\n");

        try {

            //Create a new list of student to be filled by CSV file data
            List<CrossItem> items = new ArrayList();

            //initialize FileReader object
            fileReader = new FileReader(fileName);

            //initialize CSVParser object
            csvFileParser = new CSVParser(fileReader, csvFileFormat);

            //Get a list of CSV file records
            List csvRecords = csvFileParser.getRecords();

            //Read the CSV file records starting from the second record to skip the header
            for (int i = 1; i < csvRecords.size(); i++) {
                CSVRecord record = (CSVRecord) csvRecords.get(i);
                //Create a new student object and fill his data

                CrossItem item = new CrossItem();
                item.setPartNo(record.get(0));
                item.setDescription(record.get(1));
                item.setModelNames(record.get(2));
                item.setModelCount(record.get(3));

                System.out.println(item);
                System.out.println("-------------------");
                items.add(item);
            }

            return items;
            //Print the new student list
//            for (Student student : students) {
//                System.out.println(student.toString());
//            }
        } catch (Exception e) {
            System.out.println("Error in CsvFileReader !!!");
            e.printStackTrace();
        } finally {
            try {
                fileReader.close();
                csvFileParser.close();
            } catch (IOException e) {
                System.out.println("Error while closing fileReader/csvFileParser !!!");
                e.printStackTrace();
            }
        }
        return Collections.emptyList();
    }
}
