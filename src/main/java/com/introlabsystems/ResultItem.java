package com.introlabsystems;

/**
 * Created by vitalii on 1/11/16.
 */
public class ResultItem {

    private String partNo;
    private String description;
    private String modelNumbers;
    private int count;

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getModelNumbers() {
        return modelNumbers;
    }

    public void setModelNumbers(String modelNumbers) {
        this.modelNumbers = modelNumbers;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "ResultItem{" +
                "partNo='" + partNo + '\'' +
                ", description='" + description + '\'' +
                ", modelNumbers=" + modelNumbers +
                ", count='" + count + '\'' +
                '}';
    }
}
