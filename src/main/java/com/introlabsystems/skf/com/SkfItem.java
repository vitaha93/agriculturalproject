package com.introlabsystems.skf.com;

/**
 * Created by vitalii on 1/26/16.
 */
public class SkfItem {

    private String competitorName;
    private String skfPartNumber;
    private String competitorPartNumber;

    public String getCompetitorName() {
        return competitorName;
    }

    public void setCompetitorName(String competitorName) {
        this.competitorName = competitorName;
    }

    public String getSkfPartNumber() {
        return skfPartNumber;
    }

    public void setSkfPartNumber(String skfPartNumber) {
        this.skfPartNumber = skfPartNumber;
    }

    public String getCompetitorPartNumber() {
        return competitorPartNumber;
    }

    public void setCompetitorPartNumber(String competitorPartNumber) {
        this.competitorPartNumber = competitorPartNumber;
    }
}
