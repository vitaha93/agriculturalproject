package com.introlabsystems;

import com.introlabsystems.agroparts.CrossItem;
import com.introlabsystems.amazone.Item;
import com.introlabsystems.skf.com.SkfItem;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by vitalii on 1/12/16.
 */
public class ExcelTools {

    public void writeSets(List<Item> items, String filename) throws IOException {

        ZipSecureFile.setMinInflateRatio(0);

        SXSSFWorkbook workbook = new SXSSFWorkbook();

        SXSSFSheet sheet = workbook.createSheet("items");

        SXSSFRow row = sheet.createRow(0);

        SXSSFCell cell = row.createCell(0);

        cell.setCellValue("Product type");

        cell = row.createCell(1);

        cell.setCellValue("Model Type");

        cell = row.createCell(2);

        cell.setCellValue("Model Name");

        cell = row.createCell(3);

        cell.setCellValue("Where It Fits");

        cell = row.createCell(4);

        cell.setCellValue("Additional Model Data");

        cell = row.createCell(5);

        cell.setCellValue("Part #");

        cell = row.createCell(6);

        cell.setCellValue("Description");

        cell = row.createCell(7);

        cell.setCellValue("Image");

        cell = row.createCell(8);

        cell.setCellValue("Dimension");


        for (Item item : items) {

            if (items.indexOf(item) % 10000 == 0) {
                System.out.println(items.indexOf(item));
            }

            row = sheet.createRow(items.indexOf(item) + 1);

            cell = row.createCell(0);

            cell.setCellValue(item.getProductType());

            cell = row.createCell(1);

            cell.setCellValue(item.getModelType());

            cell = row.createCell(2);

            cell.setCellValue(item.getModelName());

            cell = row.createCell(3);

            cell.setCellValue(item.getWhereItFits());

            cell = row.createCell(4);

            cell.setCellValue(item.getAddModelData());

            cell = row.createCell(5);

            cell.setCellValue(item.getPartNo());

            cell = row.createCell(6);

            cell.setCellValue(item.getDescription());

            cell = row.createCell(7);

            cell.setCellValue(item.getImage());

            cell = row.createCell(8);

            cell.setCellValue(item.getDimension());

        }

        FileOutputStream out =

                new FileOutputStream(new File("./" + filename + ".xls"));

        workbook.write(out);

        out.close();


        System.out.println(".xls crated!");

    }


    public void writeCrossReference(List<CrossItem> items, String filename) throws IOException {

        ZipSecureFile.setMinInflateRatio(0);

        SXSSFWorkbook workbook = new SXSSFWorkbook();

        SXSSFSheet sheet = workbook.createSheet("items");

        SXSSFRow row = sheet.createRow(0);

        SXSSFCell cell = row.createCell(0);

        cell.setCellValue("Part #");

        cell = row.createCell(1);

        cell.setCellValue("Description");

        cell = row.createCell(2);

        cell.setCellValue("Model names");

        cell = row.createCell(3);

        cell.setCellValue("Model Count");

        for (CrossItem item : items) {

            if (items.indexOf(item) % 10000 == 0) {
                System.out.println(items.indexOf(item));
            }

            row = sheet.createRow(items.indexOf(item) + 1);

            cell = row.createCell(0);

            cell.setCellValue(item.getPartNo());

            cell = row.createCell(1);

            cell.setCellValue(item.getDescription());

            cell = row.createCell(2);

            cell.setCellValue(item.getModelNames());

            cell = row.createCell(3);

            cell.setCellValue(item.getModelCount());
        }

        FileOutputStream out =

                new FileOutputStream(new File(filename + ".xlsx"));

        workbook.write(out);

        out.close();


        System.out.println(".xls crated!");

    }


    public void writeSkfItems(List<SkfItem> items, String filename) throws IOException {

        ZipSecureFile.setMinInflateRatio(0);

        SXSSFWorkbook workbook = new SXSSFWorkbook();

        SXSSFSheet sheet = workbook.createSheet("items");

        SXSSFRow row = sheet.createRow(0);

        SXSSFCell cell = row.createCell(0);

        cell.setCellValue("SKF Part number");

        cell = row.createCell(1);

        cell.setCellValue("Competitor part number");

        cell = row.createCell(2);


        for (SkfItem item : items) {

            if (items.indexOf(item) % 10000 == 0) {
                System.out.println(items.indexOf(item));
            }

            row = sheet.createRow(items.indexOf(item) + 1);

            cell = row.createCell(0);

            cell.setCellValue(item.getSkfPartNumber());

            cell = row.createCell(1);

            cell.setCellValue(item.getCompetitorPartNumber());

            cell = row.createCell(2);

            cell.setCellValue(item.getCompetitorName());
        }

        FileOutputStream out =

                new FileOutputStream(new File(filename + ".xlsx"));

        workbook.write(out);

        out.close();


        System.out.println(".xls crated!");

    }


    public List<SkfItem> readSkfItems() throws IOException {
        FileInputStream file = new FileInputStream(new File("/home/vitalii/Downloads/457649.xlsx"));

        List<SkfItem> skfItems = new ArrayList<>();
        //Get the workbook instance for XLS file
        XSSFWorkbook workbook = new XSSFWorkbook(file);

        //Get first sheet from the workbook
        XSSFSheet sheet = workbook.getSheetAt(0);
        String competitorName = "";
        //Iterate through each rows from first sheet
        Iterator<Row> rowIterator = sheet.iterator();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();

            //For each row, iterate through each columns
            Iterator<Cell> cellIterator = row.cellIterator();
            while (cellIterator.hasNext()) {

                Cell cell = cellIterator.next();

                String cellValue = cell.getStringCellValue();
                System.out.println(cellValue);
                if (cellValue.contains("...")) {
                    SkfItem skfItem = new SkfItem();

                    skfItem.setCompetitorPartNumber(StringUtils.substringBefore(cellValue, "..."));
                    skfItem.setSkfPartNumber(StringUtils.substringAfterLast(cellValue, "..."));
                    skfItem.setCompetitorName(competitorName);
                    skfItems.add(skfItem);
                } else {
                    competitorName = cellValue;
                }

            }
            System.out.println("");
        }
        file.close();
        return skfItems;
    }

}
