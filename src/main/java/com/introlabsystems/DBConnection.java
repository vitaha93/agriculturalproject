package com.introlabsystems;

import com.introlabsystems.amazone.Item;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vitalii on 1/11/16.
 */
public class DBConnection {
    Connection con = null;

    public void openSqlLiteConnection() {
        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:/home/vitalii/agro.db");
            System.out.println("Opened database successfully");

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

    public List<ResultItem> getPartNumbers() {
        Statement stmt = null;
        List<ResultItem> resultItems = new ArrayList<>();
        try {
            stmt = con.createStatement();
            final ResultSet rs = stmt.executeQuery("SELECT partNoWithSpaces, description, COUNT(modelName) as modelCount," +
                    "   GROUP_CONCAT(DISTINCT modelName) as modelNames" +
                    " FROM(SELECT * FROM mytable" +
                    " ORDER BY modelName)" +
                    " GROUP BY partNoWithSpaces;");
            System.out.println("partNo started");
            while (rs.next()) {
                ResultItem resultItem = new ResultItem();
                resultItem.setDescription(rs.getString("description"));
                resultItem.setPartNo(rs.getString("partNoWithSpaces"));
                resultItem.setModelNumbers(rs.getString("modelNames"));
                resultItem.setCount(rs.getInt("modelCount"));
                resultItems.add(resultItem);
            }
            rs.close();
            stmt.close();
            System.out.println("partNo finished with " + resultItems.size() + " items");
            return resultItems;
        } catch (Exception ex) {
            ex.printStackTrace();
            return resultItems;
        }
    }


    public List<Item> getItems() {
        Statement stmt = null;
        List<Item> resultItems = new ArrayList<>();
        try {
            stmt = con.createStatement();
            final ResultSet rs = stmt.executeQuery("SELECT * FROM lemken");
            System.out.println("partNo started");
            while (rs.next()) {
                Item resultItem = new Item();
                resultItem.setProductType(rs.getString("productType"));
                resultItem.setModelType(rs.getString("modelType"));
                resultItem.setModelName(rs.getString("modelName"));
                resultItem.setWhereItFits(rs.getString("whereItFits"));
                resultItem.setAddModelData(rs.getString("modelData"));
                resultItem.setDescription(rs.getString("description"));
                resultItem.setPartNo(rs.getString("partNo"));
                resultItem.setImage(rs.getString("image"));
                resultItem.setDimension(rs.getString("dimension"));
                resultItems.add(resultItem);
            }
            rs.close();
            stmt.close();
            System.out.println("partNo finished with " + resultItems.size() + " items");
            return resultItems;
        } catch (Exception ex) {
            ex.printStackTrace();
            return resultItems;
        }
    }

}